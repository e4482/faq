<?php
/**
 * // This file is part of FAQ.
 * //
 * // FAQ is free software: you can redistribute it and/or modify
 * // it under the terms of the GNU General Public License as published by
 * // the Free Software Foundation, either version 3 of the License, or
 * // (at your option) any later version.
 * //
 * // FAQ is distributed in the hope that it will be useful,
 * // but WITHOUT ANY WARRANTY; without even the implied warranty of
 * // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * // GNU General Public License for more details.
 * //
 * // You should have received a copy of the GNU General Public License
 * // along with FAQ  If not, see <http://www.gnu.org/licenses/>.
 *
 * *
 *  * @package     local_faq
 *  * @author      Charly Piva
 *  * @copyright   2018 Académie de Versailles
 *  * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

global $DB,$OUTPUT,$PAGE,$USER, $CFG,$SITE;
require_once("../../config.php");
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_url('/local/faq/index.php');
$PAGE->set_title($SITE->shortname);
$PAGE->set_heading($SITE->fullname);
$PAGE->set_pagelayout('standard');
$PAGE->requires->css(new moodle_url('/local/faq/css/faq.css'));


$params = [
    'role'      => optional_param('role', '', PARAM_TEXT),
    'element'   => optional_param('element', '', PARAM_TEXT),
    'item'      => optional_param('item', '', PARAM_TEXT),
    'page'      => optional_param('page', 'menu', PARAM_TEXT)
];
echo $OUTPUT->header();
echo '<div class="card faq">';

define('BASE_URL','https://git.backbone.education/elea/documentation/raw/master/');
//define('BASE_URL', 'http://dev02.elea.ac-versailles.fr/local/faq/test/');

(new local_faq\local\controllers\core_controller())->render($params);
$PAGE->requires->js(new moodle_url('/local/faq/js/minigrid.min.js'));
$PAGE->requires->js_call_amd('local_faq/faq', 'init');


echo '</div>';
echo $OUTPUT->footer();
exit;
























$id_actuel = 1;
$id_displayed = false;


//$Parsedown = new Parsedown();

foreach($docs as $i => $doc){
    
    //Depuis qu'il n'y a plus d'onglet, on n'affiche que la faq sélectionnée.
    if($active_type != $doc['id']) continue;
    
    $data = '';
    foreach($doc['url'] as $url){
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $base_url.$url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, false);
        $response = curl_exec($curl);
        $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        if($httpCode == 200) {
            $data .= $response."\n";
        }
        curl_close($curl);
    }

    $html = $Parsedown->text($data);
    //Ajouter un espace à la fin de chaque ligne
    $html = str_replace("\n", " \n",$html);
    //Remplacer les liens relatifs par des liens absolus
    $html = preg_replace("/(href|src)\=\"([^(http)])(\/)?/", '$1="'.$base_url.'$2', $html);

    $lines = explode("\n",$html);
    $titles = preg_grep("/<h2>([\s\S]*)<\/h2>|<h1>([\s\S]*)<\/h1>/", $lines);

    $titre_actuel = false;
    $slug_h1 = '';
    $faq_content = '';
    $faq_sommaire = [];
    $faq_sommaire_head = false;

    foreach($lines as $l => $content){
        //Si la ligne qu'on veut afficher est un titre
        if(array_key_exists($l,$titles)){
            
            //Si un titre était déjà commencé, on l'enlève en fermant éventuellement les éléments ouverts
            if($titre_actuel){
                if($titre_actuel == 'h2'){ 
                    $faq_content .= '<br/><a class="retour-sommaire" href="#"><i class="fa fa-arrow-up"></i> Retour au sommaire</a>';
                    $faq_content .= '</div>'; //Fermer faq-element
                    $faq_content .= '</div>'; //Fermer list-group-item
                }
                $titre_actuel = false; 
            }
            
            //On récupère le contenu et le type du titre
            $titre_actuel = substr($content,1,2);
            $matches = [];
            preg_match("/<h[0-9]>([\s\S]*)<\/h[0-9]>/", $content, $matches);
            $content_titre = $matches[1];
            
            switch($titre_actuel){
                //Si le titre est un h1
                case 'h1': 
                    //On ferme le list-group éventuellement ouvert par un précédent h1
                    if($faq_sommaire_head){
                        if($faq_sommaire_head['children']){
                            $faq_content .= '</div>'; //Fermer le list-group
                        }
                        $faq_sommaire[] = $faq_sommaire_head;
                        $faq_sommaire_head = false;
                    }
                    //On ferme le list-group éventuellement ouvert par un précédent h2
                    if($faq_sommaire_head === []){
                        $faq_content .= '</div>'; //Fermer le list-group
                    }
                    //On écrit le titre
                    $slug_h1 = url_slug($content_titre);
                    if($active_type == $doc['id'] && !$id_displayed && $params['partie'] == $slug_h1 && $params['question'] == ''){
                        $id_displayed = $id_actuel;
                    }
                    $faq_content .= '<h1 data-id="'.$id_actuel.'" data-slug-h1="'.$slug_h1.'" id="faq-h1-'.$id_actuel.'">'.$content_titre.'</h1>';
                    //On crée un nouvel élément de sommaire
                    $faq_sommaire_head = [
                        'id'        => $id_actuel,
                        'titre'     => $content_titre,
                        'type'      => 'h1',
                        'children'  => [],
                    ];  
                break;
                //Si le titre est un h2, on doit créer un list-group-element
                //ainsi qu'un list-group s'il n'avait pas encore été créé
                case 'h2':
                    /*
                     * Si un h1 a déjà été créé mais que le list-group n'avait pas été créé, 
                     * on crée un list-group
                     */
                    if($faq_sommaire_head && !$faq_sommaire_head['children']){ 
                        $faq_content .= '<div class="list-group">';
                    }
                    /*
                     * Si aucun h1 n'avait été créé avant, on remplace faq_sommaire_head
                     * par un tableau vide et on crée un list-group
                     */
                    if($faq_sommaire_head === false){ 
                        $faq_sommaire_head = [];
                        $faq_content .= '<div class="list-group">';
                    }
                    //Début du list-group-item et écriture du titre
                    $faq_content .= '<div class="list-group-item">';
                    $slug = 'data-slug-h1="'.$slug_h1.'" data-slug-h2="'.url_slug($content_titre).'"';
                    $faq_content .= '<h2 data-id="'.$id_actuel.'" class="list-group-item-heading" '.$slug.'  id="faq-h2-'.$id_actuel.'">';
                    $faq_content .= '<a href="#" class="faq-element-toggle" data-id="'.$id_actuel.'">';
                    $faq_content .= $content_titre;
                    $faq_content .= ' <i class="fa fa-chevron-down"></i>';
                    $faq_content .= '</a></h2>';
                    
                    /*
                     * Si un h1 a déjà débuté un élément de sommaire,
                     * on ajoute ce h2 comme sous-élément de ce sommaire,
                     * sinon ce h2 sera lui-même un élément de sommaire.
                     */
                    $faq_sommaire_element = [
                        'id'        => $id_actuel,
                        'titre'     => $content_titre,
                        'type'      => 'h2',
                        'children'  => [],
                    ];
                    if($faq_sommaire_head){
                        $faq_sommaire_head['children'][] = $faq_sommaire_element;
                    }
                    else{
                        $faq_sommaire[] = $faq_sommaire_element;
                    }
                    
                    $display_block = '';
                    if($active_type == $doc['id'] && !$id_displayed && $params['partie'] == $slug_h1 && $params['question'] == url_slug($content_titre)){
                        $id_displayed = $id_actuel;
                        $display_block = 'style="display:block"';
                    }
                    $faq_content .= '<div '.$display_block.' id="faq-element-'.$id_actuel.'" class="list-group-item-text faq-element">';
                break;
            }
            $id_actuel++;
        }
        //Si la ligne qu'on veut afficher n'est pas un titre, on l'affiche
        else{ 
            //Remplacer les liens vers scolawebTV par des vidéos intégrées
            $content = preg_replace("/<a href=\"(http:\/\/|https:\/\/|)scolawebtv.crdp-versailles.fr\/\?([\s\S]*)\">([\s\S]*)<\/a>/", '<iframe frameborder="0" width="570" height="321" src="//scolawebtv.crdp-versailles.fr/?iframe&$2" allowfullscreen ></iframe>', $content);
            $faq_content .= $content;
        }
    }
    //Fin de l'affichage de cette doc, on ferme les éléments éventuellement ouverts
    if($titre_actuel){
        if($titre_actuel == 'h2'){ 
            $faq_content .= '<br/><a class="retour-sommaire" href="#"><i class="fa fa-arrow-up"></i> Retour au sommaire</a>';
            $faq_content .= '</div>'; //Fermer faq-element
            $faq_content .= '</div>'; //Fermer list-group-item
        }
        if($faq_sommaire_head === []){
            $faq_content .= '</div>'; //Fermer le list-group
        }
        if($faq_sommaire_head){
            if($faq_sommaire_head['children']){
                $faq_content .= '</div>'; //Fermer le list-group
            }
            $faq_sommaire[] = $faq_sommaire_head;
        }
    }

    /*
     * Préparation du sommaire
     * Si le sommaire avec les sous-parties comporte plus de 15 éléments, on ne crée que les parties (les <h1>)
     */
    $faq_sommaire_content = '<h3>Sommaire</h3>';
    $faq_sommaire_content .= '<div class="faq-sommaire">';
    $nb_liens = 0;
    foreach($faq_sommaire as $elt){
        $nb_liens += 1 + count($elt['children']);
    }
    foreach($faq_sommaire as $elt){
        $faq_sommaire_content .= '<div class="faq-sommaire-cadre">';
        $faq_sommaire_content .= '<a class="faq-sommaire-titre faq-element-show" data-id="'.$elt['id'].'" href="#faq-'.$elt['type'].'-'.$elt['id'].'">';
        $faq_sommaire_content .= '<i class="fa fa-question-circle" aria-hidden="true"></i> ';
        $faq_sommaire_content .= $elt['titre'];
        $faq_sommaire_content .= '</a>'; 
        if($elt['children'] && $nb_liens < 15){
            $faq_sommaire_content .= '<ul>';
            foreach($elt['children'] as $sub_elt){
                $faq_sommaire_content .= '<li>';
                $faq_sommaire_content .= '<a class="faq-element-show" data-id="'.$sub_elt['id'].'" href="#faq-'.$sub_elt['type'].'-'.$sub_elt['id'].'">';
                $faq_sommaire_content .= '<i class="fa fa-arrow-right"></i> ';
                $faq_sommaire_content .= $sub_elt['titre'];
                $faq_sommaire_content .= '</a></li>';
            }
            $faq_sommaire_content .= '</ul>';
        }  
        $faq_sommaire_content .= '</div>'; 
    }
    $faq_sommaire_content .= '</div>';
        
    /*
     * Ecriture de la doc.
     * On n'écrit que la doc du type sélectionné plutôt que de tout écrire comme quand il y avait des onglets.
     */
    //echo '<div class="faq-doc" id="faq-doc-'.$doc['id'].'" '.($active_type == $doc['id'] ? 'style="display:block"' : '').'>';
    echo '<h1 id="faq-header" class="faq-doc-titre">Éléa - '.$doc['titre'].'</h1>';
    if(isset($doc['nom'])) echo '<a class="faq-doc-titre-retour" href="'.new moodle_url('/local/faq/index.php').'?type=menu">Retour au choix de profil</a>';
    echo $faq_sommaire_content;
    echo $faq_content;
    //echo '</div>';
}

?>
</div>

<?php 

?>
