<?php
/**
 * // This file is part of FAQ.
 * //
 * // FAQ is free software: you can redistribute it and/or modify
 * // it under the terms of the GNU General Public License as published by
 * // the Free Software Foundation, either version 3 of the License, or
 * // (at your option) any later version.
 * //
 * // FAQ is distributed in the hope that it will be useful,
 * // but WITHOUT ANY WARRANTY; without even the implied warranty of
 * // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * // GNU General Public License for more details.
 * //
 * // You should have received a copy of the GNU General Public License
 * // along with FAQ  If not, see <http://www.gnu.org/licenses/>.
 *
 * *
 *  * @package     local_faq
 *  * @author      Charly Piva
 *  * @copyright   2018 Académie de Versailles
 *  * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */
?>
<div class="faq-item">
    <?php echo $content->get_html_content(); ?>
</div>
<div class="faq-retour-sommaire">
    <a href="<?php echo new moodle_url('/local/faq/index.php?role='.$content->get_role()) ?>">
        <span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span> Retour au sommaire
    </a>
</div>
