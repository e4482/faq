<?php
/**
 * // This file is part of FAQ.
 * //
 * // FAQ is free software: you can redistribute it and/or modify
 * // it under the terms of the GNU General Public License as published by
 * // the Free Software Foundation, either version 3 of the License, or
 * // (at your option) any later version.
 * //
 * // FAQ is distributed in the hope that it will be useful,
 * // but WITHOUT ANY WARRANTY; without even the implied warranty of
 * // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * // GNU General Public License for more details.
 * //
 * // You should have received a copy of the GNU General Public License
 * // along with FAQ  If not, see <http://www.gnu.org/licenses/>.
 *
 * *
 *  * @package     local_faq
 *  * @author      Charly Piva
 *  * @copyright   2018 Académie de Versailles
 *  * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */
?>
<div class="faq-sommaire">
    <?php if ($content->has_questions_reponses()): ?>
        <div class="faq-lien-qestions-reponses faq-sommaire-element">
            <a href="<?php echo new moodle_url('/local/faq/index.php?role=' . $content->get_role() . '&page=questionsreponses')?>">
                <h3 class="faq-sommaire-element-titre">Foire aux questions</h3>
            </a>
        </div>
    <?php endif?>
        <?php foreach($content->get_elements() as $elt):?>
        <?php if($elt->titre): ?>
        <div class="faq-sommaire-wrapper">
            <div class="faq-sommaire-element">
            <?php
            if($elt->type == 'tuto'):
                $link = '/local/faq/?role='.$elt->role.'&element='.$elt->slug;
            ?>
                <a href="<?php echo new \moodle_url($link)?>">
                    <span class="glyphicon glyphicon-<?php echo $elt->glyphicon?> faq-sommaire-glyphicon-tuto" aria-hidden="true"></span>
                <h3 class="faq-sommaire-element-titre">
                <?php echo $elt->titre?>
                </h3></a>
                <p><?php echo $elt->description?></p>
            <?php
            endif;
            if($elt->type == 'categorie'):
            ?>
                <span class="glyphicon glyphicon-<?php echo $elt->glyphicon?> faq-sommaire-glyphicon-tuto" aria-hidden="true"></span>
                <h3><?php echo $elt->titre?></h3>
                <p><?php echo $elt->description?></p>
                <ul>
                <?php
                foreach($elt->items as $item){
                    $link = '/local/faq/?role='.$elt->role.'&element='.$elt->slug.'&item='.$item['slug'];
                    echo '<li><a href="'.new \moodle_url($link).'">'.$item['title'].'</a></li>';
                }
                ?>
                </ul>
            <?php endif; ?>
            </div>
        </div>
        <?php endif; ?>
        <?php endforeach; ?>
</div>