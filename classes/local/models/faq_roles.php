<?php
/**
 * // This file is part of FAQ.
 * //
 * // FAQ is free software: you can redistribute it and/or modify
 * // it under the terms of the GNU General Public License as published by
 * // the Free Software Foundation, either version 3 of the License, or
 * // (at your option) any later version.
 * //
 * // FAQ is distributed in the hope that it will be useful,
 * // but WITHOUT ANY WARRANTY; without even the implied warranty of
 * // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * // GNU General Public License for more details.
 * //
 * // You should have received a copy of the GNU General Public License
 * // along with FAQ  If not, see <http://www.gnu.org/licenses/>.
 *
 * *
 *  * @package     local_faq
 *  * @author      Charly Piva
 *  * @copyright   2018 Académie de Versailles
 *  * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

namespace local_faq\local\models;
use \local_faq\local\helpers as helpers;

class faq_roles
{
    private $selector;
    private $role_array;
    private $active;

    public function __construct($selector = ''){
        $this->load_role_array();
        $this->set_selector($selector);
    }

    public function set_selector($selector = ''){
        $this->selector = $selector;
        $this->set_active();
    }

    public function get_active(){
        return $this->active;
    }

    public function get_role_array(){
        return $this->role_array;
    }

    public function get_active_name(){
        foreach($this->role_array as $role){
            if($role['id'] == $this->get_active()){
                return $role['nom'];
            }
        }
        return false;
    }

    /*
     * Charge un tableau contenant les différents noms de rôles
     */
    private function load_role_array(){
        $helpers = helpers::get_instance();
        $roles_raw = $helpers->get_file('roles');
        $roles = [];
        foreach($roles_raw as $role){
            $roles[] = [
                'id'    => strpos($role, "=") !== false ? substr($role, 0, strpos($role, "=")) : $role,
                'nom'   => strpos($role, "=") !== false ? substr($role, strpos($role, "=")+1) : ''
            ];
        }
        $this->role_array = $roles;
    }

    /*
    * Change le rôle actif (ou le met à false si le selecteur ne correspond à aucun rôle)
    */
    private function set_active(){
        $active_role = false;
        $param = $this->selector;

        //Si la doc demandée n'est pas en paramètre, on essaie de la déterminer en fonction du profil
        if(!$param){
            if (get_capability_info('local/platformagent:school') && has_capability('local/platformagent:school', \context_system::instance())) {
                $param = 'cb';
            } else if (get_capability_info('local/trainingtroops:sandbox') && has_capability('local/trainingtroops:sandbox', \context_system::instance()) && get_config('local_trainingtroops', 'bac_on')) {
                $param = 'formateur';
            } else if (get_capability_info('local/massimilate:import') && has_capability('local/massimilate:import', \context_system::instance())) {
                $param = 'ref';
            } else if(isloggedin()){
                $param = 'prof';
            }
        }
        foreach($this->role_array as $role){
            if($role['id'] == $param){
                $active_role = $role['id'];
            }
        }
        if($param == 'menu'){
            $active_role = false;
        }
        $this->active = $active_role;
    }
}