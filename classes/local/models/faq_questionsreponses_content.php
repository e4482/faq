<?php
/**
 * Created by PhpStorm.
 * User: dane
 * Date: 12/06/2020
 * Time: 14:13
 */

namespace local_faq\local\models;


use core_user\output\myprofile\node;
use DOMDocument;
use local_faq\local\controllers\parsedown;
use local_faq\local\helpers;
use PSR4\main;

class faq_questionsreponses_question {
    private $id;
    private $questionnode;
    private $answerelements;

    public function __construct($questionnode) {
        $this->questionnode = $questionnode;
        $this->id = helpers::slugify($questionnode->textContent);
        $this->answerelements = [];
    }

    public function add_answer_element($answerelementnode) {
        $this->answerelements[] = $answerelementnode;
    }

    public function is_answered() {
        return count($this->answerelements) > 0;
    }

    public function create_node($document, $accordionid) {
        // Question/answer block
        $maindiv = $document->createElement('div');
        $maindiv->setAttribute('id', $this->id);
        $maindiv->setAttribute('class', 'panel'); // Mandatory for bootstrap accordion

        // Question div
        $questiondiv = $document->createElement('div');
        $questionlink = $document->createElement('a');
        $questionlink->setAttribute('class', 'faq-questionsreponses-question');
        $questionlink->setAttribute('data-toggle', 'collapse');
        $questionlink->setAttribute('aria-expanded', 'false');
        $questionlink->setAttribute('href', "#reponse-$this->id");
        $questionlink->setAttribute('aria-controls', "reponse-$this->id");
        $questionlink->setAttribute('data-parent', "#$accordionid");

        //$newquestionnode = $document->importNode($this->questionnode, true);
        $newquestionnode = $document->createElement('h4');
        $newquestionnode->textContent = $this->questionnode->textContent;
        $questionlink->appendChild($newquestionnode);
        $questiondiv->appendChild($questionlink);
        $maindiv->appendChild($questiondiv);

        // Answer div
        $answerdiv = $document->createElement('div');
        $answerdiv->setAttribute('id', "reponse-$this->id");
        $answerdiv->setAttribute('class', 'collapse faq-questionsreponses-reponse');
        $answerdiv->setAttribute('aria-labelledby', $this->id);
        foreach ($this->answerelements as $answerelement) {
            $newanswerelement = $document->importNode($answerelement, true);
            $answerdiv->appendChild($newanswerelement);
        }
        $maindiv->appendChild($answerdiv);

        return $maindiv;
    }
}

class faq_questionsreponses_category {

    private $questions;
    private $id;
    private $document;

    public function __construct($rawhtml, $document) {
        // Read generated HTML as DOM document
        $rawdom = new DOMDocument('1.0', 'UTF-8');
        $rawdom->loadHTML('<?xml encoding="UTF-8">' . helpers::clean_html($rawhtml, BASE_URL . 'faq/'));
        $rootnode = $rawdom->getElementsByTagName('body')->item(0);

        // Getting category properties
        $categorynamenode = $rootnode->getElementsByTagName('h2')->item(0);
        $categoryname = $categorynamenode->textContent;
        $this->id = helpers::slugify($categoryname);
        $this->document = $document;

        // Reading element by element the DOM to construct questions and answers
        $currentquestions = [];
        $this->questions = [];
        foreach ($rootnode->childNodes as $childNode) {
            if (empty($childNode->tagName)) {
                continue;
            }
            if ($childNode->tagName == 'h2') { // Category case, managed at the beginning of the function
                continue;
            } else if ($childNode->tagName == 'h3') {   // Question case
                $question = new faq_questionsreponses_question($childNode);
                // If an answer already happened, reinitialize current questions after saving them
                // else add question with same answer
                if (count($currentquestions) > 0 && $currentquestions[0]->is_answered()) {
                    $this->questions = array_merge($this->questions, $currentquestions);
                    $currentquestions = [];
                }
                $currentquestions[] = $question;

            } else {    // Answer case
                if (empty($currentquestions)) {
                    error_log("Error in category $categoryname, a question may have been skipped");
                } else {
                    foreach ($currentquestions as $question) {
                        $question->add_answer_element($childNode);
                    }

                }
            }
        }
        // Add last questions
        $this->questions = array_merge($this->questions, $currentquestions);
    }

    public function add_question_nodes($mainnode) {
        foreach ($this->questions as $question) {
            $mainnode->appendChild($question->create_node($this->document, faq_questionsreponses_content::FAQ_QUESTIONSREPONSES));
        }
    }
}

class faq_questionsreponses_content implements faq_content_interface
{
    private $role;

    private $parsedown;
    private $html;
    private $dom;
    private $maindiv;

    const FAQ_QUESTIONSREPONSES = 'faq-questionsreponses';

    public function __construct($role)
    {
        $helpers = helpers::get_instance();
        $this->role = $role;
        $this->parsedown = new Parsedown();

        $rolefile = $helpers->get_file("faq/faq_$role", true);

        $this->dom = new DOMDocument('1.0', 'UTF-8');
        $this->maindiv = $this->dom->createElement('div');
        $this->maindiv->setAttribute('id', self::FAQ_QUESTIONSREPONSES);
        $this->dom->appendChild($this->maindiv);


        foreach ($rolefile as $categoryname) {
            $categoryfile = $helpers->get_file("faq/$categoryname.md", false);
            if (empty($categoryfile)) {
                error_log("Category $categoryname not found, skipped");
            } else {
                $this->add_category($categoryfile);
            }
        }
    }

    private function add_category($categoryfile) {
        $categorytext = $this->parsedown->parse($categoryfile);
        $category = new faq_questionsreponses_category($categorytext, $this->dom);
        $category->add_question_nodes($this->maindiv);
    }

    public function get_role()
    {
        return $this->role;
    }

    public function get_html_content()
    {
        return $this->dom->saveHTML();
    }

    public function get_titre()
    {
        return "Foire aux questions";
    }

    public function get_menu_mode() {
        return false;
    }
}