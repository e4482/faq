<?php
/**
 * // This file is part of FAQ.
 * //
 * // FAQ is free software: you can redistribute it and/or modify
 * // it under the terms of the GNU General Public License as published by
 * // the Free Software Foundation, either version 3 of the License, or
 * // (at your option) any later version.
 * //
 * // FAQ is distributed in the hope that it will be useful,
 * // but WITHOUT ANY WARRANTY; without even the implied warranty of
 * // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * // GNU General Public License for more details.
 * //
 * // You should have received a copy of the GNU General Public License
 * // along with FAQ  If not, see <http://www.gnu.org/licenses/>.
 *
 * *
 *  * @package     local_faq
 *  * @author      Charly Piva
 *  * @copyright   2018 Académie de Versailles
 *  * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

namespace local_faq\local\models;
use \local_faq\local\helpers as helpers;
use \local_faq\local\controllers\parsedown;

class faq_element
{
    public $role;
    public $type;
    public $slug;
    public $items;
    public $titre;
    public $glyphicon;
    public $description;

    private $filenames;
    private $loaded;
    private $raw;

    public function __construct($type = 'tuto', $role = ''){
        $this->type = $type;
        $this->role = $role;
        $this->filenames = [];
        $this->titre = '';
        $this->slug = '';
        $this->items = [];
        $this->loaded = false;
        $this->raw = '';
        $this->glyphicon = $type == 'tuto' ? 'book' : 'list';
        $this->description = '';
    }

    public function add_filename($filename){
        $this->filenames[] = $filename;
    }

    public function set_titre($titre){
        $this->titre = $titre;
        $this->slug = helpers::slugify($this->titre);
    }

    public function set_description($desc){
        $this->description = $desc;
    }

    /*
     * Charge le contenu des fichiers de cet élément.
     * Si l'élément est un tuto, il ne contient qu'un seul item, qui est le contenu du tuto.
     * Si l'élément est une catégorie, chaque <h2> est converti en un item.
     */
    public function load_content(){
        $this->raw = '';
        $raw_files = [];
        foreach($this->filenames as $file){
            $raw_files[] = helpers::get_instance()->get_file($file,false);
        }
        $this->raw = implode("\n",$raw_files);

        $Parsedown = new Parsedown();
        //Parser contenu du tuto
        $html = $Parsedown->text($this->raw);
        $html = helpers::clean_html($html);

        //Dans le cas d'un tuto, on ne fait que charger le html dans le tableau d'items
        if($this->type == 'tuto'){
            $this->items = [$html];
        }
        //Dans le cas d'une catégorie, chaque <h2> est un item.
        if($this->type == 'categorie'){
            $this->items = [];

            $lines = explode("\n",$html);
            $titles = preg_grep("/<h2>([\s\S]*)<\/h2>/", $lines);
            $current_title = '';
            $current_slug = '';
            $current_content = '';

            foreach($lines as $l => $content) {
                //Si la ligne qu'on veut afficher est un titre
                if(array_key_exists($l,$titles)) {
                    if($current_slug)  $this->items[] = [
                        'slug'      => $current_slug,
                        'title'     => $current_title,
                        'content'   => $current_content
                    ];
                    $current_title = strip_tags($content);
                    $current_slug = helpers::slugify($current_title);
                    $current_content = '';
                }
                else{
                    $current_content .= $content;
                }
            }

            if($current_slug)  $this->items[] = [
                'slug'      => $current_slug,
                'title'     => $current_title,
                'content'   => $current_content
            ];
        }

        $this->loaded = true;
    }


    /*
     * Affiche l'élément
     */
    public function display($slug = ''){
        $html = '';
        //Dans le cas d'un tuto, rien à faire : on l'affiche
        if($this->type == 'tuto' && isset($this->items[0])){
            $html = $this->items[0];
            return $html;
        }
        //Dans le cas d'une catégorie, on cherche si le slug demandé correspond bien à un des items.
        //Si oui, on l'affiche.
        if($this->type == 'categorie'){
            foreach($this->items as $i => $item){
                if($item['slug'] == $slug) {
                    $html .= '<h2 class="faq-item-titre">'.$this->titre.' : '.$item['title'].'</h2>';

                    $nav = '';
                    if(isset($this->items[$i-1])) {
                        $prev = $this->items[$i-1];
                        $link = '/local/faq/?role='.$this->role.'&element='.$this->slug.'&item='.$prev['slug'];
                        $nav .= '<a href="'.new \moodle_url($link).'"><span class="glyphicon glyphicon-backward" aria-hidden="true"></span> '.$prev['title'].'</a>';
                    }
                    if(isset($this->items[$i+1])) {
                        $next = $this->items[$i+1];
                        $link = '/local/faq/?role='.$this->role.'&element='.$this->slug.'&item='.$next['slug'];
                        if(isset($this->items[$i-1])) $nav .= ' | ';
                        $nav .= '<a href="'.new \moodle_url($link).'">'.$next['title'].' <span class="glyphicon glyphicon-forward" aria-hidden="true"></span></a>';
                    }
                    $html .= '<div class="faq-previous-next">'.$nav.'</div>';
                    $html .= $item['content'];
                    return $html;
                }
            }
        }
        return false;
    }
}