<?php
/**
 * Created by PhpStorm.
 * User: dane
 * Date: 12/06/2020
 * Time: 14:15
 */

namespace local_faq\local\models;


interface faq_content_interface
{
    public function get_html_content();
    public function get_role();
    public function get_titre();
}