<?php
/**
 * // This file is part of FAQ.
 * //
 * // FAQ is free software: you can redistribute it and/or modify
 * // it under the terms of the GNU General Public License as published by
 * // the Free Software Foundation, either version 3 of the License, or
 * // (at your option) any later version.
 * //
 * // FAQ is distributed in the hope that it will be useful,
 * // but WITHOUT ANY WARRANTY; without even the implied warranty of
 * // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * // GNU General Public License for more details.
 * //
 * // You should have received a copy of the GNU General Public License
 * // along with FAQ  If not, see <http://www.gnu.org/licenses/>.
 *
 * *
 *  * @package     local_faq
 *  * @author      Charly Piva
 *  * @copyright   2018 Académie de Versailles
 *  * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */

namespace local_faq\local\models;
use local_faq\local\controllers\parsedown;
use \local_faq\local\helpers as helpers;

class faq_content implements faq_content_interface
{
    private $role;
    private $selector_element;
    private $selector_item;
    private $hasquestionsreponses;
    private $elements;
    private $titre;

    private $menu_mode;
    private $html_content;

    public function __construct($role = '',$selector_element = '', $selector_item = ''){
        $this->role = $role;
        $this->selector_element = $selector_element;
        $this->selector_item = $selector_item;
        $this->hasquestionsreponses = !empty(helpers::get_instance()->get_file("faq/faq_$this->role", false));
        $this->load_elements();
        $this->set_content();
    }

    public function has_questions_reponses() {
        return $this->hasquestionsreponses;
    }

    private function load_elements(){
        $this->elements = [];
        $this->titre = '';
        $elements_raw = helpers::get_instance()->get_file('role_'.$this->role,true);
        $new_element = [];
        foreach($elements_raw as $line){
            if(!$line) continue;
            //C'est un nouvel élément.
            if(substr($line,0,1) == '[' && substr($line,-1) == ']'){
                if($new_element){
                    $this->elements[] = $new_element;
                }
                $new_element = new faq_element(substr($line,1,-1), $this->role);
            }
            //C'est un titre. Si on n'a pas encore créé d'élément, c'est le titre de la FAQ.
            //Sinon, c'est le titre de l'élément.
            else if(substr(strtolower($line),0,6) == 'titre='){
                $titre = substr($line,6);
                if($new_element) $new_element->set_titre($titre);
                else $this->titre = $titre;
            }
            //C'est la description de l'élément
            else if(substr(strtolower($line),0,12) == 'description='){
                $desc = substr($line,12);
                if($new_element) $new_element->set_description($desc);
            }
            //C'est un nom de fichier.
            else if($new_element){
                $new_element->add_filename($line);
            }
        }
        if($new_element){
            $this->elements[] = $new_element;
        }
    }

    //Prépare le contenu demandé en paramètres
    private function set_content(){
        $this->menu_mode = true;
        $this->html_content = '';
        foreach($this->elements as $elt){
            $elt->load_content();
            //Chargement du contenu du tuto si les sélecteurs correspondent bien à un tuto
            if($elt->slug == $this->selector_element && $this->selector_element != ''){
                //Si on veut afficher un tuto, pas besoin de sélectionner un item.
                //On charge le contenu du tuto
                if($elt->type == 'tuto'){
                    $this->menu_mode = false;
                    $this->html_content = $elt->display();
                }
                //Si on a choisi une catégorie de FAQ, il faut que l'item choisi soit valide.
                else if($elt->type == 'categorie'){
                    $display = $elt->display($this->selector_item);
                    if($display){
                        $this->menu_mode = false;
                        $this->html_content = $display;
                    }
                }
            }
        }
    }

    public function get_html_content(){
        return $this->html_content;
    }

    public function get_elements(){
        return $this->elements;
    }

    public function get_menu_mode(){
        return $this->menu_mode;
    }

    public function get_titre(){
        return $this->titre;
    }

    public function get_role(){
        return $this->role;
    }
}