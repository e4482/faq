<?php
/**
 * // This file is part of FAQ.
 * //
 * // FAQ is free software: you can redistribute it and/or modify
 * // it under the terms of the GNU General Public License as published by
 * // the Free Software Foundation, either version 3 of the License, or
 * // (at your option) any later version.
 * //
 * // FAQ is distributed in the hope that it will be useful,
 * // but WITHOUT ANY WARRANTY; without even the implied warranty of
 * // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * // GNU General Public License for more details.
 * //
 * // You should have received a copy of the GNU General Public License
 * // along with FAQ  If not, see <http://www.gnu.org/licenses/>.
 *
 * *
 *  * @package     local_faq
 *  * @author      Charly Piva
 *  * @copyright   2018 Académie de Versailles
 *  * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 */
namespace local_faq\local\controllers;
use local_faq\local\models as faq_models;

class core_controller
{
    public function render($params){
        $roles = new faq_models\faq_roles($params['role']);
        //Si aucun rôle n'est sélectionné, affichage du menu
        if(!$roles->get_active()) {
            include('views/menu.php');
        }
        else{
            if (isset($params['page']) && $params['page'] == 'questionsreponses') {
                $content = new faq_models\faq_questionsreponses_content($roles->get_active());
                include('views/titre.php');
                include('views/content.php');
            } else {
                $content = new faq_models\faq_content($roles->get_active(),$params['element'],$params['item']);
                include('views/titre.php');
                //Si aucun item ou élément de la FAQ n'est sélectionné, affichage des éléments
                if ($content->get_menu_mode()) {
                    include('views/sommaire.php');
                } else {
                    include('views/content.php');
                }
            }
        }
    }
}